package com.bluespurs.starterkit.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bluespurs.starterkit.UnitTest;
import com.bluespurs.starterkit.service.ProductSearchService;

@Category(UnitTest.class)
public class ProductSearchControllerUnitTest extends UnitTest {

    private MockMvc mockMvc;
    
    @Mock
    private ProductSearchService searchService;

    @Before
    public void setUp() {
        super.setUp();
        mockMvc = MockMvcBuilders.standaloneSetup(new ProductSearchController(searchService)).build();
    }

    /**
     * Test the search.
     */
    @Test
    public void testRequestReturnsResult() throws Exception {
        mockMvc.perform(get("/product/search?name=ipad"))
            .andExpect(status().isOk());
    }
}
