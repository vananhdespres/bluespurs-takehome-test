package com.bluespurs.starterkit.controller.resource;

import java.math.BigDecimal;

/**
 * 
 * @author despres
 * Represents the response of the best-priced product
 * {
 *   "productName": "iPad Mini",
 *   "bestPrice": "150.00",
 *   "currency": "CAD",
 *   "location": "Walmart"}
 * 
 */
public class Product {
	private String productName;
	private BigDecimal bestPrice;
	private String currency;
	private String location	;
	private static final String DEFAULT_CURRENCY = "USD";
	
	public Product(String productName, BigDecimal bestPrice, String location) {
		super();
		this.productName = productName;
		this.bestPrice = bestPrice;
		this.currency = DEFAULT_CURRENCY;
		this.location = location;
	}
	
	public Product(String productName, BigDecimal bestPrice, String currency, String location) {
		super();
		this.productName = productName;
		this.bestPrice = bestPrice;
		this.currency = currency;
		this.location = location;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the bestPrice
	 */
	public BigDecimal getBestPrice() {
		return bestPrice;
	}

	/**
	 * @param bestPrice the bestPrice to set
	 */
	public void setBestPrice(BigDecimal bestPrice) {
		this.bestPrice = bestPrice;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	
	/**
	 * Compare the prices
	 * @param p
	 * @return
	 */
	public int compareTo(Product p) {
		return getBestPrice().compareTo(p.getBestPrice());
	}
	
}
