	package com.bluespurs.starterkit.controller.resource;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * An ordered sequence of matching products from the Bestbuy repository (containing 0 to n items)
 * 
 * @author despres
 *
 *	A typical Bestbuy API response would be:
	{
	  "from": 1,
	  "to": 10,
	  "total": 307,
	  "currentPage": 1,
	  "totalPages": 31,
	  "queryTime": "0.005",
	  "totalTime": "0.035",
	  "partial": false,
	  "canonicalUrl": "/v1/products(categoryPath.name=All Flat-Panel TVs)?show=sku,name,salePrice&sort=salesRankMediumTerm&format=json&apiKey=YourAPIKey",
	  "products": [
	    {
	      "sku": 3356036,
	      "name": "Insignia™ - 32\" Class (31-1/2\" Diag.) - LED - 720p - 60Hz - HDTV",
	      "salePrice": 179.99
	    },
	    {
	      "sku": 2563138,
	      "name": "Insignia™ - 48\" Class (47-5/8\" Diag.) - LED - 1080p - 60Hz - HDTV",
	      "salePrice": 399.99
	    }
	  ]
	 }
 * 
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class BestbuyProductList {
	
	private List<StoreProduct> products;

	/**
	 * @return the products
	 */
	public List<StoreProduct> getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public void setProducts(List<StoreProduct> products) {
		this.products = products;
	}

}
