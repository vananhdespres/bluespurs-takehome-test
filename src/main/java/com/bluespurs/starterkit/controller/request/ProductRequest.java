package com.bluespurs.starterkit.controller.request;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 
 * Validate request body of the POST request (UNIMPLEMENTED)
 * @author despres
 *
 */
public class ProductRequest {

	@NotBlank
	private String productName;
		
	@NotBlank
	@Email
	private String email;

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
}
