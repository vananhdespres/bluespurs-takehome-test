package com.bluespurs.starterkit.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bluespurs.starterkit.controller.request.RequestExceptionHandler;
import com.bluespurs.starterkit.controller.resource.Product;
import com.bluespurs.starterkit.service.ProductSearchService;

@RestController("productSearchController")
@RequestMapping("/product")
public class ProductSearchController {

	public static final Logger log = LoggerFactory.getLogger(HelloWorldController.class);

	private final ProductSearchService service;

	/**
	 * Constructor of ProductSearchController with a ProductSearchService
	 */
	@Autowired
	public ProductSearchController(ProductSearchService service) {
		this.service = service;
	}

	/**
	 * The method is mapped to "/search" as a GET request to return the lowest
	 * priced product
	 */
	@RequestMapping("/search")
	public Object search(@RequestParam String name) {
		log.info("Searching for the best priced product on Walmart and Bestbuy stores ...");

		if (StringUtils.isEmpty(name)) {
			return new RequestExceptionHandler(HttpStatus.BAD_REQUEST.value(), "the search 'name' parameter must not be blank.");
		}

		Product bestProduct = service.search(name);

		if (bestProduct == null) {
			return new RequestExceptionHandler(HttpStatus.NOT_FOUND.value(), "No matching product found from any store.");
		}

		//TODO: build a better Exception Handler so can cover other errors such as Server Error 
		
		return bestProduct;
	}
}
