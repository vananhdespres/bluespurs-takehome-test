package com.bluespurs.starterkit.controller.resource;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * The result product - an object which only contains relevant info to the client inside the list of
 * items or products
 * @author despres
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StoreProduct {

	private String name;
	private BigDecimal salePrice;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the salePrice
	 */
	public BigDecimal getSalePrice() {
		return salePrice;
	}

	/**
	 * @param salePrice the salePrice to set
	 */
	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

}
