package com.bluespurs.starterkit.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.bluespurs.starterkit.controller.resource.Product;
import com.bluespurs.starterkit.controller.resource.StoreProduct;
import com.bluespurs.starterkit.controller.resource.WalmartProductList;

/**
 * Talks to the Walmart repository to get the best priced product Follows the
 * Walmart api specification:
 * https://developer.walmartlabs.com/docs/read/Search_API
 * 
 * @author despres
 *
 */
@Repository
public class WalmartSearchRepo implements ProductSearchRepo {
	private static final Logger log = LoggerFactory.getLogger(WalmartSearchRepo.class);

	@Value("${walmart.api-key}")
	private String apiKey;

	private String url = "http://api.walmartlabs.com/v1/search?"
			+ "apiKey={apiKey}&query={name}&sort={sort}&order={order}&numItems={numItems}";

	private static final String DEFAULT_NUM_ITEMS = "1";
	private static final String WALMART = "Walmart";

	/**
	 * Query the database to find the lowest priced product
	 */
	@Override
	public Product search(String name) {
		log.info("Finding the best priced product from the Walmart repository at: " + url);

		Map<String, String> params = getUrlVariables(name);

		RestTemplate rest = new RestTemplate();

		WalmartProductList productList = rest.getForObject(url, WalmartProductList.class, params);

		// the query should only return at most one item and it should be the
		// lowest priced product
		List<StoreProduct> products = productList.getItems();

		if (products != null && !products.isEmpty()) {
			StoreProduct storeProduct = (StoreProduct) products.get(0);
			return new Product(storeProduct.getName(), storeProduct.getSalePrice(), WALMART);
		}

		return null;
	}

	private Map<String, String> getUrlVariables(String name) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("name", name);
		params.put("apiKey", apiKey);
		params.put("sort", "price");
		params.put("order", "asc");
		params.put("numItems", DEFAULT_NUM_ITEMS);
		// params.put("format", ProductDAO.DEFAULT_FORMAT);
		// Walmart api response is defaulted to json
		return params;
	}

}
