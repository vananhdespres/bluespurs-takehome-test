package com.bluespurs.starterkit.service;

import com.bluespurs.starterkit.controller.resource.Product;

public interface ProductSearchService {
	
	public Product search(String name);
}
