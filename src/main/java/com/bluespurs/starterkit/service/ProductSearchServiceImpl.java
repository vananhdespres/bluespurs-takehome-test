package com.bluespurs.starterkit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bluespurs.starterkit.controller.resource.Product;
import com.bluespurs.starterkit.repository.ProductSearchRepo;

@Service("productSearchImpl")
public class ProductSearchServiceImpl implements ProductSearchService {

	private final List<ProductSearchRepo> repositories;
	
	@Autowired
	public ProductSearchServiceImpl(List<ProductSearchRepo> repositories) {
		this.repositories = repositories;
	}
	
	@Override
	public Product search(String name) {
		Product bestProduct = null;
		for (ProductSearchRepo repo : repositories) {
			Product product = repo.search(name);
			if (product != null && (bestProduct == null || (bestProduct != null && product.compareTo(bestProduct) <= 0))) {
				bestProduct = product;
			}
		}
		
		return bestProduct;
	}
}
