	package com.bluespurs.starterkit.controller.resource;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * An ordered sequence of matching products from the the Walmart repository (containing 0 to n items)
 * 
 * @author despres
 * 
 * A typical Walmart API response would be: 
 * {...
 * items: [
		{
		itemId: 15076191,
		
		parentItemId: 15076191,
		
		name: "Apple iPod Touch 4th Generation 32GB with Bonus Accessory Kit",
		
		salePrice: 189,
		
		categoryPath: "Electronics/iPods &amp; MP3 Players/All MP3 Players",
		
		shortDescription: "The world's most popular portable gaming device is now even more fun. Listen to your favorite albums. Send text messages over Wi-Fi with iMessage. Record HD video. Make FaceTime calls. iPod touch even works with iCloud, which stores your content and pushes it to all your devices.",
		
		longDescription: "&amp;lt;br&amp;gt;&amp;lt;b&amp;gt;Apple iPod touch 32GB (4th Gen), Black:&amp;lt;/b&amp;gt;&amp;lt;ul&amp;gt;&amp;lt;li&amp;gt;Available in both black and white&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;Free text messaging over Wi-Fi with iMessage&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;Front and back cameras for HD video recording&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;FaceTime video calling&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;iOS 5 with over 200 new features&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;Game Center with more than 60 million members&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;App Store with more than 500,000 apps, including over 100,000 games and entertainment titles&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;iTunes Store with millions of songs, movies, and TV shows&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;iCloud, which wirelessly pushes your content to all your devices4&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;Rich HTML email&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;Safari web browser&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;Wi-Fi and Bluetooth wireless technology&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;Activation and setup over Wi-Fi&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;Nike+ support built in&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;Apple Earphones&amp;lt;/li&amp;gt;&amp;lt;/ul&amp;gt;&amp;lt;br&amp;gt;&amp;lt;p&amp;gt;&amp;lt;font size=&amp;quot;1px&amp;quot;&amp;gt;&amp;lt;b&amp;gt;Note:&amp;lt;/b&amp;gt;&amp;lt;/font&amp;gt;&amp;lt;/p&amp;gt;&amp;lt;ul&amp;gt;&amp;lt;li&amp;gt;&amp;lt;font size=&amp;quot;1px&amp;quot;&amp;gt;Wi-Fi Internet access is required for some features; broadband recommended; fees may apply. Some features, applications, and services are not available in all areas. Application availability and pricing are subject to change.&amp;lt;/font&amp;gt;&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;&amp;lt;font size=&amp;quot;1px&amp;quot;&amp;gt;1GB = 1 billion bytes; actual formatted capacity less.&amp;lt;/font&amp;gt;&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;&amp;lt;font size=&amp;quot;1px&amp;quot;&amp;gt;Requires a FaceTime-enabled Mac with an Internet connection or a FaceTime-enabled iOS device with a Wi-Fi connection. Not available in all areas.&amp;lt;/font&amp;gt;&amp;lt;/li&amp;gt;&amp;lt;li&amp;gt;&amp;lt;font size=&amp;quot;1px&amp;quot;&amp;gt;App count refers to the total number of apps worldwide.&amp;lt;/font&amp;gt;&amp;lt;/li&amp;gt;&amp;lt;/ul&amp;gt;&amp;lt;br&amp;gt;",
		
		... }
		]
	...}
 * 
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class WalmartProductList {
	
	private List<StoreProduct> items;
	
	/**
	 * @return the items
	 */
	public List<StoreProduct> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<StoreProduct> items) {
		this.items = items;
	}

}
