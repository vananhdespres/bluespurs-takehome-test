package com.bluespurs.starterkit.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.bluespurs.starterkit.controller.resource.BestbuyProductList;
import com.bluespurs.starterkit.controller.resource.Product;
import com.bluespurs.starterkit.controller.resource.StoreProduct;

/**
 * Talks to the Bestbuy repository to get the best priced product
 * Follows the Bestbuy api specification: https://bestbuyapis.github.io/api-documentation
 * 
 * @author despres
 *
 */
@Repository
public class BestbuySearchRepo implements ProductSearchRepo {
    private static final Logger log = LoggerFactory.getLogger(BestbuySearchRepo.class);
	
	@Value("${bestbuy.api-key}")
	private String apiKey;
	
	private String url = "http://api.bestbuy.com/v1/products(name={name}*)?"
            + "sort={sort}&show=name,salePrice&apiKey={apiKey}&pageSize={pageSize}&format=json";
	
	private static final String DEFAULT_PAGE_SIZE = "1";
	private static final String BESTBUY = "Bestbuy";
	
	/**
	 * Query the database to find the lowest priced product
	 */
	@Override
	public Product search (String name) {
		log.info("Finding the best priced product from the Bestbuy repository at: " +  url);
				
		Map<String, String> params = getUrlVariables(name);
		
		RestTemplate rest = new RestTemplate();
		
		BestbuyProductList productList = rest.getForObject(url, BestbuyProductList.class, params);
		
		//the query should only return at most one item and it should be the lowest priced product
		List<StoreProduct> products = productList.getProducts(); 
		
		if (products != null && !products.isEmpty()) {
			StoreProduct responseProduct = (StoreProduct) products.get(0);
			return new Product (responseProduct.getName(), responseProduct.getSalePrice(), BESTBUY);		
		}
		
		return null;
	}

	private Map<String, String> getUrlVariables(String name) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("name", name);
		params.put("apiKey", apiKey);
		params.put("show", "name,salePrice");
		params.put("sort", "salePrice.asc");
		params.put("pageSize", DEFAULT_PAGE_SIZE);
		params.put("format", ProductSearchRepo.DEFAULT_FORMAT);
		return params;
	}	 
	
}
