package com.bluespurs.starterkit.repository;

import com.bluespurs.starterkit.controller.resource.Product;

public interface ProductSearchRepo {
	public static final String DEFAULT_FORMAT = "json";
	
	public Product search (String name);

}
